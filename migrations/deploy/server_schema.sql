-- Deploy server:server_schema to pg

BEGIN;


-- Server table creation
CREATE TABLE Server (
  ServerId SERIAL PRIMARY KEY,
  ServerName VARCHAR(255) NOT NULL,
  ServerDescription VARCHAR(255) NOT NULL,
  ServerModelId INTEGER NOT NULL,
  ZoneId INTEGER,
  MachineId INTEGER,
  ServerConfiguration JSON,
  ServerConfigurationTD JSON,
  Automation VARCHAR(25),
  ServerEnvironment JSON,
  LastPeerConnection TIMESTAMP DEFAULT NULL
);
-- serverselection table creation
CREATE TABLE ServerSelection (
  ServerSelectionId SERIAL PRIMARY KEY,
  ServerSelectionName VARCHAR(255) NOT NULL,
  ServerSelectionDescription VARCHAR(255) NOT NULL,
  ServerSelectionServersId INTEGER [] DEFAULT '{}',
  ServerSelectionUsers hstore,
  Dynamique BOOLEAN NOT NULL,
  Requete VARCHAR(255)
);
ALTER TABLE ServerSelection ALTER COLUMN Dynamique
SET DEFAULT FALSE;


COMMIT;
