-- Revert server:server_schema from pg

BEGIN;

DROP SCHEMA server CASCADE;

COMMIT;
