-- Verify server:server_schema on pg

BEGIN;

SELECT 1/COUNT(*) FROM information_schema.schemata WHERE schema_name = 'server';
SELECT 1/COUNT(*) FROM information_schema.schemata WHERE schema_name = 'serverselection';

ROLLBACK;
