
import json
from zephir.config import ServiceConfig
from server.server import Server
from zephir.database import connect
from psycopg2.extras import DictCursor
from util import setup_module, teardown_module


def setup_function(function):
    global server, cursor, servermodelid
    cursor = CONN.cursor(cursor_factory=DictCursor)
    servermodelid = 1
    server = Server()
    server.erase_server(cursor)


def test_create_server():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    rserver2 = server.create_server(cursor, "my_beautiful_server2", "second server's description", servermodelid)
    assert rserver2['serverid'] - rserver['serverid'] == 1


def test_list_server():
    global server, cursor, servermodelid
    serverid = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    serverid2 = server.create_server(cursor, "my_beautiful_server2", "second server's description", servermodelid)
    assert len(server.list_servers(cursor)) == 2


def test_describe_server():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    assert server.describe_server(cursor, rserver['serverid'])['servername'] == 'my_beautiful_server'


def test_update_server():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    rserver2 = server.update_server(cursor, rserver['serverid'], "my server", "my server's description")
    assert rserver['serverid'] == rserver2['serverid']
    assert rserver['serverdescription'] != rserver2['serverdescription']


def test_delete_server():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    assert len(server.list_servers(cursor)) == 1
    rserver1 = server.delete_server(cursor, rserver['serverid'])
    assert rserver == rserver1
    assert len(server.list_servers(cursor)) == 0


def test_register_server_for_automation():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    assert server.register_server_for_automation(cursor, rserver['serverid'], "salt") is True


def test_get_automation_command():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    server.register_server_for_automation(cursor, rserver['serverid'], "salt")
    assert server.get_automation_command(cursor, rserver['serverid']) == ("salt", "cmd.run")

def test_get_config():
    global server, cursor, servermodelid
    rserver = server.create_server(cursor, "my_beautiful_server", "server's description", servermodelid)
    result = None
    assert server.fetch_configuration(cursor, rserver['serverid']) == result
    configuration = '{"creole.general.numero_etab": "000007", \
                      "creole.interface_0.adresse_ip_eth0": "192.168.0.24", \
                      "creole.interface_0.interface_gw": "ens4"}'
    server.update_configuration(cursor, rserver['serverid'], configuration)
    assert server.fetch_configuration(cursor, rserver['serverid']) == json.loads(configuration)
