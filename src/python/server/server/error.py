class ServerError(Exception):
    """Base class of :class:`Server` exceptions
    """


class ServerErrorPeeringConfNotAvailable(ServerError):
    """No available peering configuration
    """


class ServerErrorDatabaseNotAvailable(ServerError):
    """No available database service
    """


class ServerErrorDbConnection(ServerError):
    """Database connection error
    """


class ServerErrorInvalidServerId(ServerError):
    """Invalid server ID
    """


class ServerErrorInvalidServerModelId(ServerError):
    """Invalid servermodel ID
    """


class ServerErrorServerNameNotProvided(ServerError):
    """Servername expected
    """


class ServerErrorUnknownServerId(ServerError):
    """Unknown server ID
    """


class ServerErrorUnknownServerModelId(ServerError):
    """Unknown servermodel ID
    """


