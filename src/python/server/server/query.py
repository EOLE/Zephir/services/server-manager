from zephir.i18n import _
from .error import ServerErrorUnknownServerId

"""
List all servers
"""

FETCH_ALL_SERVERS = '''
    SELECT serverid, servername, serverdescription, servermodelid, zoneid, machineid, automation, lastpeerconnection
    FROM server
 '''

"""
Fetch one server based on its ID
"""
FETCH_SERVER_ENV = '''
    SELECT serverid, servername, serverdescription, servermodelid, zoneid, machineid, automation, serverenvironment, lastpeerconnection
    FROM server
    WHERE serverid = %s
'''
"""
Fetch one server based on its ID
"""
FETCH_SERVER = '''
    SELECT serverid, servername, serverdescription, servermodelid, zoneid, machineid, automation, lastpeerconnection
    FROM server
    WHERE serverid = %s
'''
"""
Fetch one server based on its ID with configuration
"""
FETCH_SERVER_TD_CONF = '''
    SELECT serverid, servername, serverdescription, servermodelid, zoneid, machineid, automation, lastpeerconnection, serverconfigurationtd
    FROM server
    WHERE serverid = %s
'''

"""
Fetch a server automation type
"""
FETCH_SERVER_AUTOMATION = '''
    SELECT automation
    FROM server
    WHERE serverid = %s
'''

"""
Creates a server based on its name and servermodel
"""
SERVER_INSERT = '''
    INSERT INTO server (servername, serverdescription, servermodelid)
    VALUES (%s, %s, %s)
    RETURNING *
'''

"""
Updates a server name based on its ID
"""
SERVER_UPDATE = '''
    UPDATE server
    SET servername = %s,
        serverdescription = %s
    WHERE serverid = %s
    RETURNING *
'''

"""
Set environment content for given server
"""
SERVER_UPDATE_ENVIRONMENT = '''
    UPDATE server
    SET serverenvironment = %s
    WHERE serverid = %s
'''

"""
Set last peer connection for given server
"""
SERVER_UPDATE_PEERCONNECTION = '''
    UPDATE server
    SET lastpeerconnection = %s
    WHERE serverid = %s
'''

"""
Set configuration content for given server
"""
SERVER_UPDATE_CONFIGURATION = '''
    UPDATE server
    SET serverconfiguration = %s
    WHERE serverid = %s
'''
SERVER_UPDATE_CONFIGURATION_TD = '''
    UPDATE server
    SET serverconfigurationtd = %s
    WHERE serverid = %s
'''

"""
Get configuration content for given server
"""
FETCH_SERVER_CONFIGURATION = '''
    SELECT serverconfiguration FROM server
    WHERE serverid = %s
'''

"""
Set automation field to associate server with automation software
"""
SERVER_AUTOMATION_SET = '''
    UPDATE server
    SET automation = %s
    WHERE serverid = %s
'''

"""
Deletes a server based on its ID
"""
SERVER_DELETE = '''
    DELETE
    FROM server
    WHERE serverid = %s
    RETURNING *
'''

"""
Suppression du contenu de la table de jointure servermodelapplicationservice
"""
SERVER_ERASE = """DELETE from server"""


def fetchone(cursor, query: str, query_parameters: tuple=None, raises: bool=False):
    cursor.execute(query, query_parameters)
    fetched = cursor.fetchone()
    if not fetched:
        if raises:
            raise Exception(_("cannot find element with query {} and parameters {}").format(query, query_parameters))
        fetched = None
    return fetched


def server_row_to_dict(server, serverenvironment=False):
    server_obj = {'serverid': server['serverid'],
                  'servername': server['servername'],
                  'serverdescription': server['serverdescription'],
                  'servermodelid': server['servermodelid']}
    if server['lastpeerconnection'] is None:
        server_obj['lastpeerconnection'] = ""
    else:
        server_obj['lastpeerconnection'] = str(server['lastpeerconnection'])
    if serverenvironment:
        # Cas du describe : on veut le serverenvironment
        if server['serverenvironment'] is not None:
            server_obj['serverenvironment'] = server['serverenvironment']
        else:
            server_obj['serverenvironment'] = {}
    if server['automation'] is not None:
        server_obj['automation'] = server['automation']
    if server['zoneid'] is not None:
        server_obj['zoneid'] = server['zoneid']
    if server['machineid'] is not None:
        server_obj['machineid'] = server['machineid']
    return server_obj


def list_all_servers(cursor):
    cursor.execute(FETCH_ALL_SERVERS)
    ret = []
    for server in cursor.fetchall():
        ret.append(server_row_to_dict(server))
    return ret


def fetch_server_dict(cursor, serverid: int, environment: bool):
    if environment:
        server = fetchone(cursor, FETCH_SERVER_ENV, (serverid,), raises=False)
    else:
        server = fetchone(cursor, FETCH_SERVER, (serverid,), raises=False)
    if server is None:
        raise ServerErrorUnknownServerId(_('Unable to find a server with ID {}').format(serverid))
    return server_row_to_dict(server, serverenvironment=environment)


def fetch_server(cursor, serverid: int, with_td_conf: bool=False):
    if with_td_conf:
        fetched = fetchone(cursor, FETCH_SERVER_TD_CONF, (serverid,))
    else:
        fetched = fetchone(cursor, FETCH_SERVER, (serverid,))
    if fetched is None:
        raise ServerErrorUnknownServerId(_('unable to find a server with ID {}').format(serverid))
    return fetched


def insert_server(cursor, servername, serverdescription, servermodelid):
    return server_row_to_dict(fetchone(cursor, SERVER_INSERT, (servername, serverdescription, servermodelid), raises=True))


def update_server(cursor, serverid, servername, serverdescription):
    return server_row_to_dict(fetchone(cursor, SERVER_UPDATE, (servername, serverdescription, serverid), raises=True))


def update_environment(cursor, serverid, serverenvironment):
    cursor.execute(SERVER_UPDATE_ENVIRONMENT, (serverenvironment, serverid))
    return cursor.statusmessage


def update_configuration(cursor, serverid, serverconfiguration, deploy):
    if deploy:
        cursor.execute(SERVER_UPDATE_CONFIGURATION, (serverconfiguration, serverid))
    else:
        cursor.execute(SERVER_UPDATE_CONFIGURATION_TD, (serverconfiguration, serverid))
    return cursor.statusmessage


def update_peerconnection(cursor, serverid, lastpeerconnection):
    cursor.execute(SERVER_UPDATE_PEERCONNECTION, (lastpeerconnection, serverid))
    return cursor.statusmessage


def fetch_configuration(cursor, serverid):
    fetched = fetchone(cursor, FETCH_SERVER_CONFIGURATION, (serverid,), raises=True)
    return fetched


def delete_server(cursor, serverid):
    return server_row_to_dict(fetchone(cursor, SERVER_DELETE, (serverid,), raises=True))


def set_automation_value(cursor, serverid, automation):
    cursor.execute(SERVER_AUTOMATION_SET, (automation, serverid))
    return cursor.statusmessage


def erase_server(cursor):
    """
    Supprime le contenu de la table Server
    """
    cursor.execute(SERVER_ERASE)
