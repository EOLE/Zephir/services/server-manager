from .error import ServerError
from .query import (list_all_servers,
                   fetch_server,
                   fetch_server_dict,
                   insert_server,
                   update_server,
                   update_configuration,
                   update_peerconnection,
                   update_environment,
                   fetch_configuration,
                   delete_server,
                   set_automation_value,
                   erase_server
                   )

class Server():
    """Server Manage API
    """
    #def get_columns_index_by_name(self, cursor) -> dict:
    #    """convenience method to retrieve the columns by name
    #    """
    #    cols = {}
    #    for index, col in enumerate(cursor.description):
    #        cols[col.name] = index
    #    return cols

    def list_servers(self, cursor):
        """Fetch Servers from database

        :return: list of server object
        """
        return list_all_servers(cursor)

    def describe_server(self, cursor, serverid, environment):
        """Get server information asynchronously from database

        :param `int` serverid: server identifier
        """
        return fetch_server_dict(cursor, serverid, environment)

    def create_server(self, cursor, servername, serverdescription, servermodelid):
        """Creates a server in database

        :param str serverame: server name
        :param str serverdescription: servermodel identifier
        :param int servermodelid: servermodel identifier
        :return: newly created server identifier
        :rtype: int
        """
        return insert_server(cursor, servername, serverdescription, servermodelid)

    def update_server(self, cursor, serverid, servername, serverdescription):
        """Updates a server in database

        :param int serverid: server identifier
        :param str servername: server name
        :param str serverdescription: servermodel identifier
        :return bool: True for update success, False either
        """
        return update_server(cursor, serverid, servername, serverdescription)

    def delete_server(self, cursor, serverid):
        """Deletes a server in database

        :param int serverid: server identifier
        :return bool: True for delete success, False either
        """
        return delete_server(cursor, serverid)

    def register_server_for_automation(self, cursor, serverid, automation):
        """
        Set automation field in database to associate server to automation.
        """
        return set_automation_value(cursor, serverid, automation) == 'UPDATE 1'

    def get_automation_command(self, cursor, serverid, if_peer=False, if_td_conf=False):
        """
        Get automation engine associated with serverid and return
        command suitable for this engine.
        """
        server = fetch_server(cursor, serverid, with_td_conf=if_td_conf)
        automation = server.get('automation')
        if if_peer and not server['lastpeerconnection']:
            raise ServerError('Server without peer connection')
        if if_td_conf and not server['serverconfigurationtd']:
            raise ServerError('Server without configuration')
        if not automation:
            raise ServerError('Automation engine not available, please try later')
        elif automation == 'salt':
            command = 'cmd.run'
        else:
            raise ServerError('Automation engine "{}" not supported'.format(automation))
        return automation, command

    def erase_server(self, cursor):
        erase_server(cursor)

    def update_configuration(self, cursor, serverid, configuration, deploy):
        return update_configuration(cursor, serverid, configuration, deploy) == 'UPDATE 1'

    def check_environment_modified(self, cursor, serverid, environment):
        server = self.describe_server(cursor, serverid, environment=True)
        return server['serverenvironment'] != environment

    def update_environment(self, cursor, serverid, environment):
        return update_environment(cursor, serverid, environment) == 'UPDATE 1'

    def update_peerconnection(self, cursor, serverid, lastpeerconnection):
        return update_peerconnection(cursor, serverid, lastpeerconnection) == 'UPDATE 1'

    def fetch_configuration(self, cursor, serverid):
        return fetch_configuration(cursor, serverid)[0]
